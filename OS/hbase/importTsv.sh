#!/usr/bin/env bash

hbase org.apache.hadoop.hbase.mapreduce.ImportTsv -Dimporttsv.columns=HBASE_ROW_KEY,cf:c1,cf:c2,cf:c3,cf:c4,cf:c5,cf:c6,cf:c7,cf:c8,cf:c9,cf:c10 -Dimporttsv.skip.bad.lines=true '-Dimporttsv.separator=|' -Dimporttsv.bulk.output=hdfs://hbase-master:9000/tmp/bktableoutput bktable hdfs://hbase-master:9000/tmp/longkeydata